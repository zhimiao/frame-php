<?php
/**
 * pdo强化类
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-11-09
 * Time: 17:39
 */
namespace zhimiao\Data;

class XPDO extends \PDO {

    /**
     * 批量的从$aPrepareSource中绑定预处理值到$stmt对象中
     *
     * @param &PDOStatement $stmt
     * @param array $aPrepareSource [
     *									SQL参数名，参数名对应的值，值的类型
     *								]
     * @return void
     */
    public function batchBindValue(XPDOStatement &$stmt, array $param)
    {
        foreach ($param as $strKey => $aItem) {
            $aItem = (array) $aItem;
            if (!isset($aItem[1])) {
                $aItem[1] = XPDO::PARAM_STR;
            }
            $stmt->bindValue($strKey, $aItem[0], $aItem[1]);
        }
        unset($stmt);
    }

    /**
     * 快速使用SQL预处理执行SQL语句
     * 注：返回的记录集不用时要记得注销 eg:$recordset = null
     *
     * @param string $strSql
     * @param array $param
     * @param boolean $bExec 如果为true则调用PODStatement::execute不返回记录集
     * @return XPDOStatement
     */
    public function quickPrepare($strSql, array $param, $bExec = false)
    {
        $statement = null;
        $statement = $this->prepare($strSql);
        $this->batchBindValue($statement, $param);
        if ($bExec) {
            $statement = $statement->execute();
        }
        else {
            $statement->execute();
        }
        return $statement;
    }
}

class XPDOStatement extends \PDOStatement implements \Traversable, \Countable {

    public $pdo = null;

    protected function __construct(XPDO $pdo = null)
    {
        if (!is_null($pdo)) {
            $this->pdo = $pdo;
        }
    }

    /**
     * 返回数据，根据需要释放游标
     *
     * Lamb_Db_RecordSet_Interface implemention
     * @param boolean $isAutoClose 是否需要释放游标 默认为false
     *
     * @return array
     */
    public function toArray($isAutoClose = true)
    {
        $ret = $this->fetchAll();
        if ($isAutoClose && !$this->nextRowset()) {
            $this->closeCursor();
        }
        return $ret;
    }

    /**
     * 获取单条记录
     * @param int $index
     * @param null $field
     * @return null
     */
    public function getOnce($field = null)
    {
        $ret = $this->fetchAll();
        $this->closeCursor();
        if (isset($ret[0])) {
            if (!empty($field)) {
                return isset($ret[0][$field]) ? $ret[0][$field] : false;
            }
            return $ret[0];
        }
        return false;
    }

    /**
     * Countable implemention
     *
     * @return int
     */
    public function count()
    {
        return $this->getRowCount();
    }
}