<?php
/**
 * 纸喵php框架总入口
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-06
 * Time: 17:46
 */
namespace zhimiao;

// 定义纸喵php框架根目录
define('ZHIMIAO_ROOT', __DIR__);
defined('ROOT_PATH') or define('ROOT_PATH', ZHIMIAO_ROOT. '/../../..');

class Run {
    public function __construct()
    {
        Config::init();
        Route::init();
        $Reflection = new Reflection();
        $param = array_merge(Request::get(), Request::post());
        $data = $Reflection->invokeMethod(array_values(Route::getMeta()), $param);
        if(!is_null($data)) {
            call_user_func_array(["\zhimiao\Response", "json"], (array) $data);
        }
    }
}