<?php
/**
 * 纸喵php框架配置类
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-06
 * Time: 17:42
 */
namespace zhimiao;

class Config {
    // 初始化配置模板
    public static $config = [
        'is_dev' => true,
        'log_name' => 'zhimiaoFrame',
        'log_topic' => 'zhimiao',
        'db' => [
            'dsn' => '',
            'user' => '',
            'password' => ''
        ],
        'redis' => [
            'port'=> '6379', // 端口号
            'host' => '',
            'auth' => '',
        ],
        'server_cfg_path' => '/usr/share/nginx/server.ini',
        'cfg_map' => [
            'db' => 'database',
            'redis' => 'redis'
        ],
    ];

    /**
     * 初始化配置
     */
    public static function init() {
        if (!file_exists(ROOT_PATH. '/config.php')) {
            throw new Exception("配置文件异常", 1);
        }
        $conf = include(ROOT_PATH. '/config.php');
        self::$config = array_merge(self::$config, $conf);
        # 服务器环境注入配置
        $conf = self::$config['server_cfg_path'] ?? ''; # 地址配置同名为了减少自定义变量污染
        if(file_exists($conf)) {
            $conf = @parse_ini_file($conf, true);
            self::$config['is_dev'] = false;
        } else {
            self::$config['log_topic'] .= '(dev)';
            $conf = [];
        }
        foreach (self::$config['cfg_map'] as $k => $v) {
            $k = explode('|', $k);
            $v = explode('|', $v);
            if(isset($k[1])) {
                if(isset($conf[$v[0]][$v[1]])) {
                    self::$config[$k[0]][$k[1]] = $conf[$v[0]][$v[1]];
                }
            } elseif(isset($conf[$v[0]])) {
                self::$config[$k[0]] = $conf[$v[0]];
            }
        }
        unset($conf);
    }

    /**
     * 获取配置
     * @param string $name 配置键名支持二级.
     * @param null $value
     * @return bool|null
     */
    public static function get($name = '', $value = null) {
        // 非二级配置时直接返回
        if (!strpos($name, '.')) {
            $name = strtolower($name);
            return self::$config[$name] ?? null;
        }
        // 二维数组设置和获取支持
        $name    = explode('.', $name, 2);
        $name[0] = strtolower($name[0]);
        return self::$config[$name[0]][$name[1]] ?? null;
    }

    /**
     * 检查配置是否存在
     * @param $name
     * @return bool
     */
    public static function has($name)
    {
        if (!strpos($name, '.')) {
            return isset(self::$config[strtolower($name)]);
        }
        // 二维数组设置和获取支持
        $name = explode('.', $name, 2);
        return isset(self::$config[strtolower($name[0])][$name[1]]);
    }

    /**
     * 修改配置
     * @param $name
     * @param $value
     */
    public static function set($name, $value)
    {
        // 动态改配置
        if (!strpos($name, '.')) {
            self::$config[strtolower($name)] = $value;
        } else {
            // 二维数组
            $name = explode('.', $name, 2);
            self::$config[strtolower($name[0])][$name[1]] = $value;
        }
    }
}