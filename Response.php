<?php
/**
 *
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-07
 * Time: 10:58
 */

namespace zhimiao;

class Response {

    // 默认的错误码提示信息
    public static $code_msg = [
        0 => '系统繁忙',
        -1 => '未登录',
    ];

    /**
     * json输出
     * @param $code
     * @param null $data
     * @param string $msg
     * @param array $header
     */
    public static function json($code, $data = null, $msg = '', $header = [])
    {
        $result = [
            'code' => $code,
            'msg' => self::$code_msg[$code] ?? $msg
        ];
        if (!is_null($data)) {
            $result['result'] = $data;
        }
        @ob_clean();
        header("Content-Type: application/json; charset=utf-8");
        foreach ($header as $k => $v) {
            header("{$k}: {$v}");
        }
        echo json_encode($result);
        exit();
    }
}