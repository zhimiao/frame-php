<?php
/**
 *
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-07
 * Time: 10:16
 */

namespace zhimiao;
class Request {

    /**
     * 获取GET类型数据
     * @param null $key
     * @param null $default
     * @return null
     */
    public static function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $_GET;
        }
        return $_GET[$key] ?? $default;
    }

    /**
     * 获取POST类型数据
     * @param null $key
     * @param null $default
     * @return null
     */
    public static function post($key = null, $default = null)
    {
        if (is_null($key)) {
            return $_POST;
        }
        return $_POST[$key] ?? $default;
    }

    /**
     * 获取原始请求数据
     * @return bool|string
     */
    public static function raw()
    {
        return file_get_contents("php://input");
    }

    /**
     * 获取Header头
     * @param string $name
     * @param null $default
     * @return array|false|null
     */
    public static function header($name = '', $default = null)
    {
        $header = [];
        if (function_exists('apache_request_headers') && $result = apache_request_headers()) {
            $header = $result;
        } else {
            $header = $_SERVER;
            foreach ($_SERVER as $key => $val) {
                if (0 === strpos($key, 'HTTP_')) {
                    $key = str_replace('_', '-', strtolower(substr($key, 5)));
                    $header[$key] = $val;
                }
            }
            if (isset($_SERVER['CONTENT_TYPE'])) {
                $header['content-type'] = $_SERVER['CONTENT_TYPE'];
            }
            if (isset($_SERVER['CONTENT_LENGTH'])) {
                $header['content-length'] = $_SERVER['CONTENT_LENGTH'];
            }
        }
        $header = array_change_key_case($header);
        if (is_array($name)) {
            return $header = array_merge($header, $name);
        }
        if ('' === $name) {
            return $header;
        }
        $name = str_replace('_', '-', strtolower($name));
        return $header[$name] ?? $default;
    }

    /**
     * 获取客户端的ip
     * @return string
     */
    public static function getIp(){
        //判断服务器是否允许$_SERVER
        $realip = '';
        if(isset($_SERVER)){
            if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }elseif(isset($_SERVER["HTTP_CLIENT_IP"])) {
                $realip = $_SERVER["HTTP_CLIENT_IP"];
            }elseif(isset($_SERVER["REMOTE_ADDR"])) {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        }
        if (empty($realip)) {
            //不允许就使用getenv获取
            if(getenv("HTTP_X_FORWARDED_FOR")){
                $realip = getenv( "HTTP_X_FORWARDED_FOR");
            }elseif(getenv("HTTP_CLIENT_IP")) {
                $realip = getenv("HTTP_CLIENT_IP");
            }else{
                $realip = getenv("REMOTE_ADDR");
            }
        }
        return (string) $realip;
    }
}