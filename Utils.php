<?php
/**
 * 工具类
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-07
 * Time: 13:25
 */
namespace zhimiao;

use \zhimiao\Config;

class Utils {

    /**
     * 内置日志调试方法
     * @param string $msg
     * @param bool $lock
     */
    public static function L($msg = '', $lock = false) {
        $msg = @json_encode($msg);
        @file_put_contents(
            ROOT_PATH. '/log/'. Config::get('log_name'). date('_Ymd'). '.txt',
            "\n[". date('H:i:s'). "] : ". $msg,
            FILE_APPEND
        );
        if($lock) {
            exit;
        }
    }

    /**
     * 调试方法
     * @param $data
     */
    public static function D($data)
    {
        if(is_array($data)) {
            print_r($data);
        } elseif(is_bool($data)) {
            echo 'this values is';
            echo $data ? 'true' : 'false';
        } else {
            var_dump($data);
        }
        exit();
    }

    /**
     * 缓存锁 - 此方法需要配合计划任务解决死锁问题
     * @param string $key 键名
     * @param bool $begin true-上锁 false-解锁
     * @param int $expire 超时时间
     * @return bool 伪布尔
     */
    public static function cacheLock($key = '', $begin = true, $expire = 300)
    {
        $cache = Data::redis();
        $key = 'cacheLock:'. $key;
        // 上锁
        if($begin) {
            return $cache->setnx($key, time() + $expire);
        } else {
            return $cache->del($key);
        }
    }

    /**
     * 加密
     * @param {string} $encryptStr 要加密的字符串
     * @param {string} $localIV 加密的向量
     * @param {string} $encryptKey 加密的密钥
     * @return {string}
     */
    static public function encrypt($encryptStr)
    {
        return openssl_encrypt($encryptStr, Config::get('openssl_secret.method'), Config::get('openssl_secret.key'), 0, Config::get('openssl_secret.iv'));
    }

    /**
     * 解密
     * @param {string} $encryptStr 要解密的字符串
     * @param {string} $localIV 解密的向量
     * @param {string} $encryptKey 密钥
     * @return {string}
     */
    static public function decrypt($encryptStr)
    {
        return openssl_decrypt($encryptStr, Config::get('openssl_secret.method'), Config::get('openssl_secret.key'), 0, Config::get('openssl_secret.iv'));
    }
}