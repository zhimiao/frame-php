<?php
/**
 * 数据
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-07
 * Time: 11:21
 */
namespace zhimiao;
class Data {

    private static $pdo = null, $redis = null;

    /**
     * mysql
     * @throws Exception
     * @return Data\XPDO
     */
    public static function pdo() {
        if (is_object(self::$pdo)) {
            return self::$pdo;
        }
        try{
            self::$pdo = new Data\XPDO(
                Config::get('db.dsn'),
                Config::get('db.user'),
                Config::get('db.password'),
                [
                    Data\XPDO::ATTR_DEFAULT_FETCH_MODE => Data\XPDO::FETCH_NAMED,
                    Data\XPDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES'utf8mb4';"
                ]
            );
            self::$pdo->setAttribute(Data\XPDO::ATTR_STATEMENT_CLASS, ['\zhimiao\Data\XPDOStatement', [self::$pdo]]);
        }catch (\Exception $e){
            throw new \Exception("数据库链接失败", 1);
        }
        return self::$pdo;
    }

    /**
     * redis
     * @return \Redis
     * @throws Exception
     */
    public static function redis() {
        if (is_object(self::$redis)) {
            return self::$redis;
        }
        try{
            # 初始化redis
            self::$redis = new \Redis();
            self::$redis->connect(Config::get('redis.host'), Config::get('redis.port')); //php客户端设置的ip及端口
            self::$redis->auth(Config::get('redis.auth')); //授权
        }catch (\Exception $e){
            throw new \Exception("缓存链接异常", 1);
        }
        return self::$redis;
    }
}
