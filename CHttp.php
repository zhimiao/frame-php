<?php
/**
 * 增强版的http请求类，支持http,https，相比Lamb_Http速度快
 * 该类是一个静态类
 */
namespace zhimiao;

class CHttp
{
	/**
	 * 通过CURL发送HTTP请求
	 * @param string $url 请求的URL地址
	 * @param array $params POST所需要的数据
	 * @param array $headers 设置的请求头
	 * @param array $option 设置 = array(
	 * 		timeout => int 超时时间，单位为毫秒
	 * 		is_get_headers => int 是否获取响应头，如果获取响应头的返回值为array
	 * 		max_redirect_nums => int 最大重定向的次数，默认为0
	 *		is_head_request => false 
	 * 		useragent => 设置客户端识别码
	 * 		referer => 来源地址
	 * )
	 * 
	 * @return string | array = {
	 * 		'headers' => array 响应头 
	 * 		'status' => int http状态码
	 * 		'content' => string 响应正文
	 * }
	 */	
	public static function core($url, $params = null, $headers = null, $option = null)
	{
		if (!is_array($option)) {
			$option = array();
		}
		
		$ch = curl_init();  
		curl_setopt($ch, CURLOPT_URL, $url);  
		curl_setopt($ch, CURLOPT_HEADER, isset($option['is_get_headers']) ? intval($option['is_get_headers']) : 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		
		if (isset($option['max_redirect_nums'])) {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, intval($option['max_redirect_nums']));
		}
		
		if (is_array($headers))	{
			$newHeaders = array();
			foreach ($headers as $k => $v) {
				$newHeaders[] = "{$k}: {$v}";
			}
			curl_setopt($ch, CURLOPT_HTTPHEADER, $newHeaders);
		} 	
		
		if ($params) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);		
		}
		
		if (isset($option['timeout'])) {
            curl_setopt($ch, CURLOPT_NOSIGNAL, true);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS, $option['timeout']);
		}
		
		if (isset($option['is_head_request']) && $option['is_head_request']) {
			curl_setopt($ch, CURLOPT_NOBODY, true);
		}

/*		
		if(isset($option['useragent'])) {
			curl_setopt($ch, CURLOPT_USERAGENT, $option['useragent']);
		}

		if(isset($option['referer'])) {
			if(empty($option['referer'])) {
				$option['referer'] = $url;
			}
			curl_setopt($ch, CURLOPT_REFERER, $option['referer']);
		}
*/
		
		$res = curl_exec($ch);
		
		if (isset($option['is_get_headers']) && $option['is_get_headers']) {
			$resHeaderSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$resHeadersStr = substr($res, 0, $resHeaderSize);
			$res = substr($res, $resHeaderSize);
			$resHeaders = array();
			
			foreach (explode("\r\n", $resHeadersStr) as $it) {
				$it = trim($it);
				if (empty($it)) {
					continue;
				}
				
				$pos = strpos($it, ': ');
				if ($pos === false) {
					continue;
				}
				$resHeaders[substr($it, 0, $pos)] = substr($it, $pos + 2);
			}
			
			$res = array(
				'status' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
				'headers' => $resHeaders,
				'content' => $res
			);
		}
		return $res;
	}	

	/**
	 * 快速发送GET请求
	 * @param string $url 请求的URL地址
	 * @param array $headers 请求头
	 * @param boolean $isAsync 是否为异步
	 * @return string
	 */
	public static function get($url, $headers = null, $isAsync = false)
	{
		$option = array(
			'max_redirect_nums' => 2,
			'is_get_headers' => 1
		);
		
		if ($isAsync) {
			$option['timeout'] = 1001;
		}
		$ret = self::core($url, null, $headers, $option);
		
		if ($ret['status'] != 200) {
			return null;
		}
		return $ret['content'];	
	}

	/**
	 * 快速发送POST请求
	 * @param string $url 请求的URL地址
	 * @param array $params post要用的数据
	 * @param array $headers 请求头
	 * @param boolean $isAsync 是否为异步
	 * @return string
	 */	
	public static function post($url, $params, $headers = null, $isAsync = false)
	{
		$option = array(
			'is_get_headers' => 1
		);
		
		if ($isAsync) {
			$option['timeout'] = 1001;
		}
		$ret = self::core($url, $params, $headers, $option);
		
		if ($ret['status'] != 200) {
			return null;
		}
		return $ret['content'];			
	}
	
	/**
	 * 获取302状态跳转的URL地址
	 * @param string $url url地址
	 * @param array $headers 设置的http头
	 */
	public static function get302Location($url, $headers = null)
	{
		$option = array(
			'is_get_headers' => 1
		);	
		$ret = self::core($url, null, $headers, $option);
		if (($ret['status'] == 302 || $ret['status'] == 301) && isset($ret['headers']['Location']) && Lamb_Utils::isHttp($ret['headers']['Location'])) {
			return $ret['headers']['Location'];
		}	
		
		return null;
	}	
}