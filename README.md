# 纸喵迷你PHP框架

> 运行环境要求PHP7.0+。

## 安装

基础项目结构

/app/api - 接口controller
/site/index.php - 入口文件
/config.php - 配置文件
/composer.json

使用composer安装

~~~
composer require zhimiao/frame-php
~~~

示例项目地址：

[https://gitee.com/zhimiao/frame-php-demo/](https://gitee.com/zhimiao/frame-php-demo/)


结构介绍

```
zhimiao/frame-php
    │  .gitignore
    │  CHttp.php - 一个迷你的curl请求库
    │  composer.json - 这个就不说了
    │  composer.phar - 这个也没啥好说的
    │  Config.php - 配置加载和读取，这里内置了服务器环境监测，用于切换配置
    │  Data.php - 数据获取，目前支持mysql和reids
    │  README.md - 说明书
    │  Reflection.php - 类反射，用于加载controller（很方便的那种，抄自TP）
    │  Request.php - 处理http请求数据的
    │  Response.php - 输出数据，里面目前就放了一个json的输出（够用了）
    │  Route.php - 路由规则目前内置  c=controller a=action
    │  Run.php - 入口
    │  Utils.php - 工具类，里面放了几个不常用的工具    │          
    └─Data
            XPDO.php - Data类中对于PDO的强化，毕竟参数绑定什么的很烦，这个就很简单了

```