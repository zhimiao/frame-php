<?php
/**
 * 路由解析
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-08-07
 * Time: 11:54
 */
namespace zhimiao;

class Route {
    public static $c = null, $a = null;
    /**
     * 初始化路由解析
     * @throws \Exception
     */
    public static function init()
    {
        self::$c = Request::get('c', 'index');
        self::$a = Request::get('a', 'index');
        $class = self::$c = "app\\api\\". self::$c;
        unset($_GET['c']);
        unset($_GET['a']);
    }

    /**
     * 获取解析的控制器/操作
     * @return array
     */
    public static function getMeta()
    {
        return [
            'c' => self::$c,
            'a' => self::$a
        ];
    }
}